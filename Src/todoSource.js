﻿toDoList = [
    {
        "id": 0,
        "title": "TODO Test Task A",
        "summary": "Test Task A",
        "status": "UpComing",
        "tags": ["test", "qa"]
    },
    {
        "id": 1,
        "title": "TODO Test Task B",
        "summary": "Test Task B",
        "status": "UpComing",
        "tags": ["test", "staging"]
    },
    {
        "id": 2,
        "title": "TODO Develop Task A",
        "summary": "Develop Task A",
        "status": "InProgress",
        "tags": ["dev"]
    },
    {
        "id": 3,
        "title": "TODO Develop Task B",
        "summary": "Develop Task B",
        "status": "InProgress",
        "tags": ["dev", "staging"]
    },
    {
        "id": 4,
        "title": "TODO Estimate Task A",
        "summary": "Estimate Task A",
        "status": "Done",
        "tags": ["dev"]
    },
    {
        "id": 5,
        "title": "TODO Estimate Task B",
        "summary": "Test Task B",
        "status": "Done",
        "tags": ["dev", "staging"]
    }
];

