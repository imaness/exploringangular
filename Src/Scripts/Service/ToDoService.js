﻿(function () {
    var ToDoService = function ($http, $filter) {
        var self = this;
        self.toDos = toDoList;
        
        var getToDoList = function () {
            return self.toDos;
        };
        
        var addToDo = function (todo) {
            self.toDos.push(todo);
        };

        var getToDos = function () {
            return self.toDos;
        };

        var editToDo = function (item) {
            self.toDos[item.id] = item;
        };

        var removeToDo = function (item) {
            self.toDos = _.without(self.toDos, _.findWhere(self.toDos, { id: item.id }));
        };

        return {
            getToDoList: getToDoList,
            addToDo: addToDo,
            getToDos: getToDos,
            editToDo: editToDo,
            removeToDo: removeToDo
        };
    };
    myApp.factory('ToDoService', ['$http', '$filter', ToDoService]);
}());